#include <QPainter>
#include <QDebug>
#include <QMouseEvent>
#include <QResizeEvent>

#include "gamewidget.h"

int GameWidget::line = 0;
QSet<QChar> GameWidget::inexistentChars;
QMap<int, QPair<QChar, GameWidget::State>> GameWidget::existentChars;

GameWidget::GameWidget(QWidget *parent) : QWidget(parent)
{
    for(int row = 0; row < RowCount; ++row){
        OneLine line; line.reserve(ColCount);
        for(int col = 0; col < ColCount; ++col){
            line.append(OneBlock());
        }
        wordsGrid_.insert(row, line);
    }
}

int GameWidget::getLine()
{
    return line;
}

void GameWidget::setLine(int newLine)
{
    if(newLine >= 0 && newLine < RowCount)
        line = newLine;
}

void GameWidget::addExistentChar(int index, const QChar &letter, State state)
{
    // 如果字母 letter 在不存在的字母列表中出现, 则无法添加入确定存在的字母列表中
    if(inexistentChars.contains(letter)) return;

    existentChars.insert(index, QPair<QChar, State>(letter, state));
}

QPair<QChar, GameWidget::State> GameWidget::getExistenChar(int index)
{
    return existentChars.value(index, QPair<QChar, State>(QChar::Null, UnKnown));
}

void GameWidget::addInexistentChar(const QChar &letter)
{
    // 如果字母 letter 在确定存在的字母列表中出现, 则无法添加入不存在的字母列表中
    for(auto ch : qAsConst(existentChars)){
        if(ch.first == letter)
            return;
    }
    inexistentChars.insert(letter);
}

QSet<QChar> GameWidget::getInexistentChar()
{
    return inexistentChars;
}

void GameWidget::clear()
{
    line = 0;
    inexistentChars.clear();
    existentChars.clear();
    for(int row = 0; row < RowCount; ++row){
        for(int col = 0; col < ColCount; ++col){
            wordsGrid_[row][col].letter = QChar::Null;
            wordsGrid_[row][col].state  = UnKnown;
        }
    }
    update();
}

void GameWidget::delWord()
{
    for(int i = 0; i < ColCount; ++i){
        wordsGrid_[line][i].letter = QChar::Null;
        wordsGrid_[line][i].state  = UnKnown;
    }
    update();
}

bool GameWidget::setLetter(int index, const QChar &letter)
{
    if(index >= ColCount) return false;
    auto upper = letter.toUpper();

    wordsGrid_[line][index].letter = upper;
    wordsGrid_[line][index].state  = UnKnown;
    if(inexistentChars.contains(upper))
        wordsGrid_[line][index].state = Gray;
    else if(existentChars.value(index).first == upper)
        wordsGrid_[line][index].state = existentChars.value(index).second;
    update();
    return true;
}

bool GameWidget::setWord(const QByteArray &word)
{
    if(word.size() != ColCount) return false;
    auto upper = word.toUpper();

    for(int i = 0; i < ColCount; ++i){
        wordsGrid_[line][i].letter = upper.at(i);
        wordsGrid_[line][i].state  = UnKnown;

        if(inexistentChars.contains(upper.at(i)))
            wordsGrid_[line][i].state = Gray;
        else if(existentChars.value(i).first == upper.at(i))
            wordsGrid_[line][i].state = existentChars.value(i).second;
    }
    update();
    return true;
}

const QByteArray GameWidget::word()
{
    QByteArray ba;
    auto oneline = wordsGrid_.value(line);
    for(auto block : qAsConst(oneline)){
        ba.append(block.letter.toLatin1());
    }
    return ba;
}

const GameWidget::OneLine GameWidget::getOneLine(int line) const
{
    return wordsGrid_.value(line);
}

bool GameWidget::event(QEvent *event)
{
    if(event->type() == QEvent::Resize){
        auto e = static_cast<QResizeEvent*>(event);
        if(e->oldSize() != e->size()){
            QRectF blockRectF = QRectF(0, 0,
                                      GridWidth * ColCount + SpaceWidget * (ColCount - 1),
                                      GridWidth * RowCount + SpaceWidget * (RowCount - 1));
            blockRectF.moveCenter(rect().center());
            QPointF origin = blockRectF.topLeft();
            for(int row = 0; row < RowCount; ++row){
                for(int col = 0; col < ColCount; ++col){
                    wordsGrid_[row][col].rect.moveTo(origin.x() + col * GridWidth + SpaceWidget * (col - 1),
                                                     origin.y() + row * GridWidth + SpaceWidget * (row - 1));
                }
            }
        }
    }
    else if(event->type() == QEvent::MouseButtonPress){
        auto e = static_cast<QMouseEvent*>(event);
        if(e->button() == Qt::LeftButton){
            for(int col = 0; col < ColCount; ++col){
                if(wordsGrid_[line][col].rect.contains(e->pos())){
                    if(wordsGrid_[line][col].letter.isNull())
                        return true;
                    if(inexistentChars.contains(wordsGrid_[line][col].letter))
                        return true;
                    else if(existentChars.value(col).first == wordsGrid_[line][col].letter)
                        return true;

                    switch (wordsGrid_[line][col].state) {
                        case Gray:
                            wordsGrid_[line][col].state = Yellow;
                            break;
                        case Yellow:
                            wordsGrid_[line][col].state = Green;
                            break;
                        case Green:
                        default:
                            wordsGrid_[line][col].state = Gray;
                            break;
                    }
                    repaint();
                }
            }
            int col = 0;
            for(col = 0; col < ColCount; ++col){
                if(wordsGrid_[line][col].state != Green)
                    break;
            }
            if(col == ColCount)
                emit allGreen(true);
            else
                emit allGreen(false);
            return true;
        }
    }

    return QWidget::event(event);
}

void GameWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);

    QPen pen;
    pen.setColor(QColor(58,58,60));
    pen.setWidth(2);
    pen.setJoinStyle(Qt::MiterJoin);

    for(const auto &line : qAsConst(wordsGrid_)){
        for(const auto &block : qAsConst(line)){
            painter.setPen(pen);
            switch (block.state) {
                case Gray:
                    painter.fillRect(block.rect, QColor(58,58,60));
                    break;
                case Yellow:
                    painter.fillRect(block.rect, QColor(181,159,59));
                    break;
                case Green:
                    painter.fillRect(block.rect, QColor(83,141,78));
                    break;
                default:
                    painter.drawRect(block.rect);
                    break;
            }

            QFont font;
            font.setFamily("Microsoft YaHei");
            font.setPixelSize(30);
            painter.setFont(font);
            painter.setPen(QColor(215,218,220));
            painter.drawText(block.rect, Qt::AlignHCenter | Qt::AlignVCenter, block.letter);
        }
    }
}
