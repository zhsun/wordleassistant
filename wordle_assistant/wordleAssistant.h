#ifndef WORDLEASSISTANT_H
#define WORDLEASSISTANT_H

#include <QMap>
#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class WordleAssistant; }
QT_END_NAMESPACE

class WordleAssistant : public QWidget
{
    Q_OBJECT

public:
    typedef QPair<QString, QString>  Word;
    typedef QList<Word>              Dictionary;

    explicit WordleAssistant(QWidget *parent = nullptr);
    ~WordleAssistant();

protected:
    bool event(QEvent *event);

private:
    void playing();

    QList<QString> splitCSV(const QString &s, const char &c = ',');
    void loadWordCSV();

    void makeCards();
    void drawCard(bool norepeat = false);

signals:
    void gameOver();

private:
    Ui::WordleAssistant *ui;

    Word theWord_;
    Dictionary dict_;
    Dictionary candidateDict_;
};

#endif // WORDLEASSISTANT_H
