#ifndef GAMEWIDGET_H
#define GAMEWIDGET_H

#include <QMap>
#include <QSet>
#include <QWidget>

#define ColCount    (5)
#define RowCount    (6)
#define GridWidth   (60)
#define SpaceWidget (7)

class GameWidget : public QWidget
{
    Q_OBJECT
public:
    enum State{
        UnKnown,
        Gray,    // The letter is not in the word in any spot.
        Yellow,  // The letter is in the word but in the wrong spot.
        Green,   // The letter is in the word and in the correct spot.
    };
    struct OneBlock{
        QRectF rect;
        QChar  letter;
        State  state;
        OneBlock(): rect(QRectF(0,0,GridWidth,GridWidth)),letter(QChar::Null),state(UnKnown){}
    };
    typedef QList<OneBlock> OneLine;

    explicit GameWidget(QWidget *parent = nullptr);

    static int getLine();
    static void setLine(int newLine);

    static void addExistentChar(int index, const QChar &letter, State state);
    static QPair<QChar, State> getExistenChar(int index);
    static void addInexistentChar(const QChar &letter);
    static QSet<QChar> getInexistentChar();

    void clear();

    void delWord();
    bool setLetter(int index, const QChar &letter);
    bool setWord(const QByteArray &word);
    const QByteArray word();

    const OneLine getOneLine(int line) const;

signals:
    void allGreen(bool);

protected:
    bool event(QEvent *event);
    void paintEvent(QPaintEvent *);

private:
    static int line;
    static QSet<QChar> inexistentChars;
    static QMap<int, QPair<QChar, State>> existentChars; // <index, <char, state>>
    QMap<int, OneLine> wordsGrid_;
};

#endif // GAMEWIDGET_H
