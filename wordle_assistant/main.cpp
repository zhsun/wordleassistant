#include "wordleassistant.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    WordleAssistant w;
    w.show();
    return a.exec();
}
