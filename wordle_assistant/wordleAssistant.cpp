#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QDebug>
#include <QKeyEvent>
#include <QRandomGenerator>
#include <QGuiApplication>
#include <QScreen>

#include "wordleAssistant.h"
#include "ui_wordleAssistant.h"

#define StarCSV    "/csv/stardict.csv"
#define WordleCSV  "/csv/wordle.csv"

WordleAssistant::WordleAssistant(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::WordleAssistant)
{
    ui->setupUi(this);

    qApp->setStyleSheet("*{font-family: \"Microsoft YaHei\"; font-weight: bold; background-color: #121213; color: #d7dadc} QPushButton[flat=\"true\"]{border: none; font-size: 14px; background-color: #818384; } QPushButton[flat=\"true\"]:hover{border: none; background-color: #97979c; } QPushButton[flat=\"true\"]:pressed{border: none; background-color: #747677; padding-top: 2px; padding-left: 2px; }");

    connect(ui->refreshButton, &QPushButton::clicked, this, &WordleAssistant::drawCard);
    connect(ui->playButton,    &QPushButton::clicked, this, &WordleAssistant::playing);
    connect(ui->gameWidget,    &GameWidget::allGreen, this, [this](bool value){
        if(ui->playButton->text() != "PLAY"){
            if(value)
                ui->playButton->setText("AGAIN");
            else if(GameWidget::getLine() != RowCount - 1)
                ui->playButton->setText("NEXT");
        }
    });
    connect(this,              &WordleAssistant::gameOver, this, [this](){
       ui->playButton->setText("AGAIN");
    });

    candidateDict_.reserve(100);
    loadWordCSV();


    QRect screenRect = QGuiApplication::primaryScreen()->geometry();
    this->move(screenRect.width()/2 - this->size().width()/2,
               screenRect.height()/2 - this->size().height()/2);
}

WordleAssistant::~WordleAssistant()
{
    delete ui;
}

bool WordleAssistant::event(QEvent *event)
{
    if(event->type() == QEvent::KeyPress){
        int key = static_cast<QKeyEvent*>(event)->key();
        if(key == Qt::Key_Return){
            playing();
        }
        else {
            static int n = ColCount;

            if(ui->playButton->text() != "PLAY"){
                if(key >= Qt::Key_A && key <= Qt::Key_Z){
                    if(n == ColCount){
                        n = 0;
                        ui->gameWidget->delWord();
                    }

                    ui->gameWidget->setLetter(n++, key);
                }
            }
        }
    }

    return QWidget::event(event);
}

void WordleAssistant::playing()
{
    auto reset = [this](){
        Word w;
        theWord_.swap(w);
        candidateDict_.clear();
        ui->recommendTextEdit->clear();
        ui->candidateTextEdit->clear();
        ui->gameWidget->clear();
    };

    if(ui->playButton->text() == "PLAY"){
        reset();
        makeCards();
        ui->playButton->setText("NEXT");
    }
    else if(ui->playButton->text() == "NEXT"){
        auto oneline = ui->gameWidget->getOneLine(GameWidget::getLine());
        auto index = 0;
        for(auto block : qAsConst(oneline)){
            switch (block.state) {
                case GameWidget::Green:{
                    GameWidget::addExistentChar(index, block.letter, block.state);
                    break;
                }
                case GameWidget::Yellow:{
                    auto l = GameWidget::getExistenChar(index);
                    if(l.second != GameWidget::Green){
                        GameWidget::addExistentChar(index, block.letter, block.state);
                    }
                    break;
                }
                case GameWidget::Gray:{
                    GameWidget::addInexistentChar(block.letter);
                    break;
                }
                default:{
                    QMessageBox::information(nullptr, "Info", "There are unprocessed block!");
                    return;
                }
            }

            ++index;
        }

        auto currLine = GameWidget::getLine() + 1;
        GameWidget::setLine(currLine);
        if(currLine == RowCount - 1)
            ui->playButton->setText("AGAIN");

        makeCards();
    }
    else if(ui->playButton->text() == "AGAIN"){
        reset();
        ui->playButton->setText("PLAY");
    }
}

QList<QString> WordleAssistant::splitCSV(const QString &s, const char &c)
{
    auto string_counter = [](const QString &str, const QString &sub)->uint{
        uint count = 0, index = 0;
        while( (index = str.indexOf(sub, index)) < (uint)str.size() ){
            index += sub.size(); ++count;
        }
        return count;
    };

    auto find_validpos = [=](const QString &s, const char &c, int start_pos = 0)->int{
        int pos1 = start_pos;
        int pos2 = s.indexOf(c, pos1);
        unsigned int count = 0;
        while( -1 != pos2 ){
            count += string_counter(s.mid(pos1, pos2 - pos1), "\"");
            if(count & 0x1){
                pos1 = pos2 + 1; // c sizeof 1
                pos2 = s.indexOf(c, pos1);
            }
            else
                break;
        }
        return pos2;
    };

    QList<QString> v;
    int pos2 = s.indexOf(c);
    int pos1 = 0;

    while (-1 != pos2){
        pos2 = find_validpos(s, c, pos1);
        if(-1 == pos2){
            break;
        }

        auto t = s.mid(pos1, pos2 - pos1);
        t.replace("\"", "");
        v.append(t);
        pos1 = pos2 + 1;
        pos2 = s.indexOf(c, pos1);
    }

    if (pos1 != s.length()){
        auto t = s.mid(pos1);
        t.replace("\"", "");
        v.append(t);
    }
    return v;
}

void WordleAssistant::loadWordCSV()
{
    // | 字段        | 解释                                                        |
    // | ----------- | ---------------------------------------------------------- |
    // | 00: word        | 单词名称                                                     |
    // | 01: phonetic    | 音标，以英语英标为主                                           |
    // | 02: definition  | 单词释义（英文），每行一个释义                                   |
    // | 03: translation | 单词释义（中文），每行一个释义                                   |
    // | 04: pos         | 词语位置，用 "/" 分割不同位置                                  |
    // | 05: collins     | 柯林斯星级                                                   |
    // | 06: oxford      | 是否是牛津三千核心词汇                                         |
    // | 07: tag         | 字符串标签：zk/中考，gk/高考，cet4/四级 等等标签，空格分割          |
    // | 08: bnc         | 英国国家语料库词频顺序                                         |
    // | 09: frq         | 当代语料库词频顺序                                             |
    // | 10: exchange    | 时态复数等变换，使用 "/" 分割不同项目，见后面表格                   |
    // | 11: detail      | json 扩展信息，字典形式保存例句（待添加）                          |
    // | 12: audio       | 读音音频 url （待添加）                                        |
    QFile file(QCoreApplication::applicationDirPath() + WordleCSV);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QMessageBox::critical(nullptr, "Error", "File does not exist.");
        return;
    }

    Dictionary d;
    dict_.swap(d);
    dict_.reserve(55000);

    QTextStream in(&file);
    in.setCodec("UTF-8");
    for(int i = 0; !in.atEnd(); i++){
        auto list = splitCSV(in.readLine());
        if(list.size() >= 12){
            auto word = list.at(0);
            auto tran = list.at(3);

#if 0
            if(!list.at(8).isEmpty() && !list.at(9).isEmpty() &&
               (list.at(8) != "0" || list.at(9) != "0")){
                //qDebug() << word << list.at(8) << list.at(9);
                dict_.append(Word{word, tran});
            }
#else
            dict_.append(Word{word, tran});
#endif
        }
    }

    file.close();
}

void WordleAssistant::makeCards()
{
    if(candidateDict_.isEmpty()){
        candidateDict_ = dict_;
    }
    else{
        auto inexistentChars = GameWidget::getInexistentChar();
        for(auto iter = candidateDict_.begin(); iter != candidateDict_.end(); ){
            bool retain = true;
            // 剔除包含不存在字母的单词
            for(auto c : qAsConst(inexistentChars)){
                if(iter->first.contains(c, Qt::CaseInsensitive)){
                    iter = candidateDict_.erase(iter);
                    retain = false;
                    break;
                }
            }

            if(retain){
                for(int i = 0; i < ColCount; ++i){
                    auto letter = GameWidget::getExistenChar(i);
                    if(letter.second == GameWidget::Yellow){
                        if(!iter->first.contains(letter.first, Qt::CaseInsensitive)){
                            iter = candidateDict_.erase(iter);
                            retain = false;
                            break;
                        }
                        if(letter.first.toUpper() == iter->first.at(i).toUpper()){
                            iter = candidateDict_.erase(iter);
                            retain = false;
                            break;
                        }
                    }
                    else if(letter.second == GameWidget::Green){
                        if(letter.first.toUpper() != iter->first.at(i).toUpper()){
                            iter = candidateDict_.erase(iter);
                            retain = false;
                            break;
                        }
                    }
                }

                if(retain)
                    ++iter;
            }
        }
    }

    if(candidateDict_.isEmpty())
        emit gameOver();

    ui->candidateTextEdit->clear();
    QString html = "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">p, li { white-space: pre-wrap; }</style></head><body style=\" font-family:\"Microsoft YaHei\"; font-size:10pt; font-weight:400; font-style:normal;\">";
    for(int i = 0; i < 100; ++i){
        if(i >= candidateDict_.size()) break;
        auto word = candidateDict_.at(i);
        html.append(QString("<span style=\" font-size:16pt; color:#538d4e;\">%1</span></p><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; color:#d7dadc;\">%2</span></p>").arg(word.first, word.second));
    }
    html.append("</body></html>");
    ui->candidateTextEdit->setHtml(html);

    drawCard(true);
}

void WordleAssistant::drawCard(bool norepeat)
{
    auto hasRepeatLetter = [](const QString &word)->bool{
        QList<QChar> list;
        for(auto letter : word){
            if(list.contains(letter)){
                return true;
            }
            list.append(letter);
        }
        return false;
    };

    if(candidateDict_.isEmpty()){
        QString html = QString("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">p, li { white-space: pre-wrap; }</style></head><body style=\" font-family:\"Microsoft YaHei\"; font-size:10pt; font-weight:400; font-style:normal;\"><span style=\" font-size:16pt; font-style: italic; color:#d7dadc;\">[null]</span></p></body></html>");
        ui->recommendTextEdit->setHtml(html);
        return ;
    }

    int n = 0;
    while(n++ < 100){
        theWord_ = candidateDict_.at(QRandomGenerator::global()->bounded(candidateDict_.size()));

        if(norepeat && hasRepeatLetter(theWord_.first))
            continue;
        else
            break;
    }

    QString html = QString("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">p, li { white-space: pre-wrap; }</style></head><body style=\" font-family:\"Microsoft YaHei\"; font-size:10pt; font-weight:400; font-style:normal;\"><span style=\" font-size:16pt; color:#538d4e;\">%1</span></p><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; color:#d7dadc;\">%2</span></p></body></html>").arg(theWord_.first, theWord_.second);
    ui->recommendTextEdit->setHtml(html);

    ui->gameWidget->setWord(theWord_.first.toLatin1());
}

